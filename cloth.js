var heightOfCloth = 30;
var widthOfCloth = 30;
var pointsCount = heightOfCloth * widthOfCloth;
var middleIndex = Math.floor(heightOfCloth / 2)* widthOfCloth +  Math.floor(widthOfCloth/2);

var stretchMinValue = 0.5;
var stretchMaxValue = 2.0;

var stepX = 0.5;
var stepZ = 0.5;

function isEpsilon(number){ 
	return Math.abs(number) < 1e-10; 
} 

// предпочтительнее полноценный вектор из мат.библиотеки, конечно, но пойдет и такой наивный	
class Vector3d {
	//поля: x y z 
	// методы: сложить, вычесть, 
	// умножить на число 
	// разделить на число 
	constructor(x,y,z) {
		this.data = new Array(3);
		this.data[0] = x;
		this.data[1] = y;
		this.data[2] = z;
	};
	add(another) {
		this.data[0] += another.data[0];
		this.data[1] += another.data[1];
		this.data[2] += another.data[2];
	};
	sub(another) {
		this.data[0] -= another.data[0];
		this.data[1] -= another.data[1];
		this.data[2] -= another.data[2];
	}
	multiply(value) {
		this.data[0] *= value;
		this.data[1] *= value;
		this.data[2] *= value;
	}
	divide(value) {
		this.data[0] /= value;
		this.data[1] /= value;
		this.data[2] /= value;
	}
	set(x,y,z) {
		this.data[0] = x;
		this.data[1] = y;
		this.data[2] = z;
	}
	distance(vector) {
		let result = 0;
		let x = this.data[0]-vector.data[0];
		let y = this.data[1]-vector.data[1];
		let z = this.data[2]-vector.data[2];
		result = x* x + y*y + z*z;
		result = Math.sqrt(result);
		return result;
	}
	length() {
		return this.distance(new Vector3d(0,0,0));
	}
	normalize() {
		this.divide(this.length());
	}
	setX(value) {
		this.data[0] = value;
	}
	setY(value) {
		this.data[1] = value;
	}
	setZ(value) {
		this.data[2] = value;
	}

	getX() {
		return this.data[0];
	}
	getY() {
		return this.data[1];
	}
	getZ() {
		return this.data[2];
	}
	cross(vector) {
		var x = this.data[1] * vector.data[2] - this.data[2] * vector.data[1];
		var y = this.data[2] * vector.data[0] - this.data[0] * vector.data[2];
		var z = this.data[0] * vector.data[1] - this.data[1] * vector.data[0];
		return new Vector3d(x,y,z);
	}
	dot(other) {
		return this.getX()*other.getX()+this.getY()*other.getY()+this.getZ()*other.getZ();
	}
	isNotNaN() {
		return !isNaN(this.data[0]) || !isNaN(this.data[1]) || !isNaN(this.data[2]);
	}
}


// матрица 4x4 для
var m4 = {
 
	identify: function() {
		return [
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0, 
			0, 0, 0, 1
		];
	},
	multiply: function(a, b) {
		var b00 = b[0 * 4 + 0];
		var b01 = b[0 * 4 + 1];
		var b02 = b[0 * 4 + 2];
		var b03 = b[0 * 4 + 3];
		var b10 = b[1 * 4 + 0];
		var b11 = b[1 * 4 + 1];
		var b12 = b[1 * 4 + 2];
		var b13 = b[1 * 4 + 3];
		var b20 = b[2 * 4 + 0];
		var b21 = b[2 * 4 + 1];
		var b22 = b[2 * 4 + 2];
		var b23 = b[2 * 4 + 3];
		var b30 = b[3 * 4 + 0];
		var b31 = b[3 * 4 + 1];
		var b32 = b[3 * 4 + 2];
		var b33 = b[3 * 4 + 3];
		var a00 = a[0 * 4 + 0];
		var a01 = a[0 * 4 + 1];
		var a02 = a[0 * 4 + 2];
		var a03 = a[0 * 4 + 3];
		var a10 = a[1 * 4 + 0];
		var a11 = a[1 * 4 + 1];
		var a12 = a[1 * 4 + 2];
		var a13 = a[1 * 4 + 3];
		var a20 = a[2 * 4 + 0];
		var a21 = a[2 * 4 + 1];
		var a22 = a[2 * 4 + 2];
		var a23 = a[2 * 4 + 3];
		var a30 = a[3 * 4 + 0];
		var a31 = a[3 * 4 + 1];
		var a32 = a[3 * 4 + 2];
		var a33 = a[3 * 4 + 3];
 
		return [
			b00 * a00 + b01 * a10 + b02 * a20 + b03 * a30,
			b00 * a01 + b01 * a11 + b02 * a21 + b03 * a31,
			b00 * a02 + b01 * a12 + b02 * a22 + b03 * a32,
			b00 * a03 + b01 * a13 + b02 * a23 + b03 * a33,
			b10 * a00 + b11 * a10 + b12 * a20 + b13 * a30,
			b10 * a01 + b11 * a11 + b12 * a21 + b13 * a31,
			b10 * a02 + b11 * a12 + b12 * a22 + b13 * a32,
			b10 * a03 + b11 * a13 + b12 * a23 + b13 * a33,
			b20 * a00 + b21 * a10 + b22 * a20 + b23 * a30,
			b20 * a01 + b21 * a11 + b22 * a21 + b23 * a31,
			b20 * a02 + b21 * a12 + b22 * a22 + b23 * a32,
			b20 * a03 + b21 * a13 + b22 * a23 + b23 * a33,
			b30 * a00 + b31 * a10 + b32 * a20 + b33 * a30,
			b30 * a01 + b31 * a11 + b32 * a21 + b33 * a31,
			b30 * a02 + b31 * a12 + b32 * a22 + b33 * a32,
			b30 * a03 + b31 * a13 + b32 * a23 + b33 * a33,
		];
	},
	transpose: function (matrix) {
		return [
		    matrix[0], matrix[4], matrix[8], matrix[12],
			matrix[1], matrix[5], matrix[9], matrix[13],
			matrix[2], matrix[6], matrix[10], matrix[14],
			matrix[3], matrix[7], matrix[11], matrix[15],
		]
	},
	ortho: function(left,right,bottom,topcoord, near, far) {
		var rl = 1/ (right - left);
		var tb = 1/ (topcoord - bottom);
		var fn = 1 / (near - far);
		return [
		2 * rl, 0, 0, 0,
		0, 2 * tb, 0, 0,
		0, 0, -2 * fn, 0,
		(right + left) * rl, (topcoord + bottom) * tb, (far + near) * fn , 1
		];
	},
	perspective: function(fov, aspect, near, far) {
		var zoom = 1.0 / Math.tan(fov / 2);
		var range = 1 / (near - far);
		return m4.transpose([
		zoom / aspect, 0, 0, 0,
		0, zoom, 0, 0,
		0, 0, (far + near) / (near - far), -1,
		0, 0, 2 * far * near / (near - far), 0,
		]);
	},
	lookAt: function(eye,center,up) {
		var zAxis = new Vector3d(0,0,0);
		zAxis.add(center);
		zAxis.sub(eye);
		zAxis.normalize();
		var normalUp = new Vector3d(0,0,0);
		normalUp.add(up);
		normalUp.normalize();
		var xAxis = zAxis.cross(normalUp);
		var yAxis = xAxis.cross(zAxis);
		var result = [
			xAxis.getX(),xAxis.getY(),xAxis.getZ(),-eye.getX(),
			yAxis.getX(),yAxis.getY(),yAxis.getZ(),-eye.getY(),
			- zAxis.getX(),- zAxis.getY(),- zAxis.getZ(),-eye.getZ(),
			0, 0, 0, 1
		];
		return m4.transpose(result);
	}
};

var gravity = new Vector3d(0,9.8,0);
var isGravityActive = false;

class Force{
	// поле: гравитация
	// можно вынести во внешнее 
	// поле: получить текущий импульс
	// поле: обнулить импульс (он обработал свое, все)
	constructor() {
		this.impulse = new Vector3d(0,0,0);
	}
	getImpulse() {
		return this.impulse;
	}
	setImpulse(x,y,z) {
		this.impulse.set(x,y,z);
	}
	addImpulse(vector) {
		this.impulse.add(vector);
	}
	getValue() {
		let result = new Vector3d(0,0,0);
		result.add(this.impulse);
		if(isGravityActive) {
			result.add(gravity);
		}
		return result;
	}
}

var onePointMass = 1.0;
class Point{
	// поля: координаты
	// связанные пружины
	// поле: базовые координаты
	// метод: интеграция верле
	constructor() {
		this.currentLocation = new Vector3d(0,0,0);
		this.prevLocation = new Vector3d(0,0,0);
		this.baseLocation = new Vector3d(0,0,0);
		this.innerForce = new Force();
		this.normal = new Vector3d(0,1,0);
		this.canMove = true;
		this.index = 0;
		this.forceMoved = false;
		this.connectedIndices = [];
	}
	setBaseLocation(vector) {
		this.baseLocation.set(0,0,0);
		this.baseLocation.add(vector);
		this.prevLocation.set(0,0,0);
		this.prevLocation.add(vector);
		this.currentLocation.set(0,0,0);
		this.currentLocation.add(vector);
		
	}
	setLocation(vector) {
		this.prevLocation.set(vector.getX(),vector.getY(),vector.getZ());
		this.currentLocation.set(vector.getX(),vector.getY(),vector.getZ());
		this.forceMoved = true;
	}
	proceed(time) {
		if(!this.canMove) return false;
		let acceleration = new Vector3d(0,0,0);
		acceleration.add(this.innerForce.getValue());
		acceleration.divide(onePointMass);
		
		this.innerForce.setImpulse(0,0,0);
		var result = new Vector3d(0,0,0);
		
		result.add(acceleration);
		result.multiply(time*time);

		// a*t^2
		result.add(this.currentLocation);
		result.add(this.currentLocation);		
		result.sub(this.prevLocation);
		// x *2 - xprev + at^2
		this.prevLocation.set(this.currentLocation.getX(),this.currentLocation.getY(),this.currentLocation.getZ());
		this.currentLocation.set(result.getX(),result.getY(),result.getZ());
		return true;
	}
	distance(otherPoint) {
		return this.currentLocation.distance(otherPoint.currentLocation);
	}
	addImpulse(vector) {
		this.innerForce.addImpulse(vector);
	}
	setCanMove(boolValue) {
		this.canMove = boolValue;
	}	
	getCanMove() {
		return this.canMove;
	}
	getCanMoveStretch() {
		if (this.forceMoved) return false;	
		return this.canMove;
	}	
	getCurrentLocation() {
		return this.currentLocation;
	}
	getBaseLocation() {
		return this.baseLocation;
	}
	getForceMoved() {
		return this.forceMoved;
	}
	clearForceMoved() {
		this.forceMoved = false;
	}
	addConnected(index) {
		this.connectedIndices = this.connectedIndices.concat(index);
	}
	collide(otherPointIndex, otherPoint) {
		if (this.connectedIndices.indexOf(otherPointIndex) != -1) return;
		if (this.distance(otherPoint) > 0.25) return;
		this.innerForce.setImpulse(0,0,0);
		otherPoint.innerForce.setImpulse(0,0,0);
		this.setLocation(this.prevLocation);
		otherPoint.setLocation(otherPoint.prevLocation);
	}
	getNormal() {
		return this.normal;
	}
	addNormal(value) {
		this.normal.add(value);
	}
	clearNormal() {
		this.normal.set(0,0,0);
	}
}



class Triangle{
	constructor(index1, index2, index3)  {
		this.firstIndex = index1;
		this.secondIndex = index2;
		this.thirdIndex = index3;
	}
	calculateNormal() {
		var first = new Vector3d(0.0,0.0,0.0);
		first.add(allPoints[this.thirdIndex].getCurrentLocation());
		first.sub(allPoints[this.firstIndex].getCurrentLocation());
		var second = new Vector3d(0.0,0.0,0.0);
		second.add(allPoints[this.secondIndex].getCurrentLocation());
		second.sub(allPoints[this.firstIndex].getCurrentLocation());
		var normal = first.cross(second);
		normal.normalize();
		allPoints[this.firstIndex].addNormal(normal);
		allPoints[this.secondIndex].addNormal(normal);
		allPoints[this.thirdIndex].addNormal(normal);
	}
	getFirst() {
		return this.firstIndex;
	}
	getSecond() {
		return this.secondIndex;
	}
	getThird() {
		return this.thirdIndex;
	}
	
}
var allPoints = new Array(widthOfCloth * heightOfCloth);

var stiffness = 5;
class Spring {
	// базовая длина
	// метод: обработать сжатие пружины
	constructor() {
		this.baseLength = 0;
		this.indexFirstPoint = 0;
		this.indexSecondPoint = 0;
		this.currentLength = 0;
	}
	setPoints(index1, index2) {
		this.indexFirstPoint = index1;
		this.indexSecondPoint = index2;
		this.baseLength = allPoints[index1].distance(allPoints[index2]);
		this.currentLength = this.baseLength;
	}
	proceed(time) {
		this.currentLength = allPoints[this.indexFirstPoint].distance(allPoints[this.indexSecondPoint]);
		let difference = this.baseLength - this.currentLength;
		if(isEpsilon(difference)) { 
			difference = 0;
		}
		let hookeCoefficient = stiffness * difference;
		let response = new Vector3d(0,0,0);
		response.add(allPoints[this.indexFirstPoint].getCurrentLocation());
		response.sub(allPoints[this.indexSecondPoint].getCurrentLocation());
		response.normalize();
		response.multiply(hookeCoefficient);
		if(response.isNotNaN()) {
			allPoints[this.indexFirstPoint].addImpulse(response);
			response.multiply(-1.0);
			allPoints[this.indexSecondPoint].addImpulse(response);		
		}
	}
	// для того, чтобы не растягивались в бесконечность: 
	// с гравитацией/ветром и одним закрепленным углом 
	// были проблемы
	stretch() {
		// temp = secod-first
		let tempNewVector = new Vector3d(0,0,0);
		tempNewVector.add(allPoints[this.indexSecondPoint].getCurrentLocation());
		tempNewVector.sub(allPoints[this.indexFirstPoint].getCurrentLocation());

		this.currentLength = tempNewVector.length();
		
		tempNewVector.normalize();
		tempNewVector.multiply(this.baseLength);				
		// если растянуто слишком сильно		
		if(this.currentLength > stretchMaxValue * this.baseLength) {			
			tempNewVector.multiply(stretchMaxValue);
			if(allPoints[this.indexFirstPoint].getCanMoveStretch()) {
				// first = second - temp
				tempNewVector.multiply(-1);
				tempNewVector.add(allPoints[this.indexSecondPoint].getCurrentLocation());
				allPoints[this.indexFirstPoint].setLocation(tempNewVector);
			} else if (allPoints[this.indexSecondPoint].getCanMoveStretch()) {
				// second = first + temp
				tempNewVector.add(allPoints[this.indexFirstPoint].getCurrentLocation());
				allPoints[this.indexSecondPoint].setLocation(tempNewVector);
			}
		} else 
			// сжато слишком сильно
			if (this.currentLength < stretchMinValue * this.baseLength) {
				tempNewVector.multiply(stretchMinValue);
				if(allPoints[this.indexFirstPoint].getCanMoveStretch()) {
					tempNewVector.multiply(-1);
					tempNewVector.add(allPoints[this.indexSecondPoint].getCurrentLocation());				
					allPoints[this.indexFirstPoint].setLocation(tempNewVector);				
				} else if (allPoints[this.indexSecondPoint].getCanMoveStretch()) {
					tempNewVector.add(allPoints[this.indexFirstPoint].getCurrentLocation());
					allPoints[this.indexSecondPoint].setLocation(tempNewVector);
				
			}	
		}
		this.currentLength = allPoints[this.indexFirstPoint].distance(allPoints[this.indexSecondPoint]);
	}
	getFirstPointIndex() {
		return this.indexFirstPoint;
	}
	getSecondPointIndex() {
		return this.indexSecondPoint;
	}
	getLengthDiffCoef() {
		let difference = Math.abs(this.currentLength - this.baseLength);
		if(isEpsilon(difference)) { 
			difference = 0.0;
		}
		let result = Math.abs(this.baseLength - difference) / (2.0* this.baseLength) + 0.5;
		return result;
	}
}

// horisontal springs: cols -1 * rows size
// vertical springs: cols * rows-1 size 
// diagonal springs: cols-1 * rows -1 
// sum: cols * rows - rows + cols * rows - cols + cols * rows - cols - rows +1 
// = 3 * rows*cols - 2 * rows - 2 * cols + 1
var springsLength = pointsCount * 3 - heightOfCloth * 2 - widthOfCloth * 2 + 1;
var allSprings = new Array(springsLength);

function initPoints() {
	let index = 0;
	let tempCoord = new Vector3d(0,0,0);
	for(let i = 0 ; i < heightOfCloth; ++i) {
		for(let j = 0; j < widthOfCloth; ++j, ++index) {
			allPoints[index] = new Point();
			allPoints[index].setBaseLocation(tempCoord);
			tempCoord.add(new Vector3d(stepX,0,0));
			allPoints[index].setCanMove(true);
			allPoints[index].index = index;
			if((i == 0 || i == heightOfCloth -1) && (j == 0 || j == widthOfCloth - 1)) {
				allPoints[index].setCanMove(false);
			}}
		tempCoord.add(new Vector3d(- widthOfCloth * stepX,0,stepZ));
	}
	allPoints[middleIndex].setCanMove(false);
}

function initSprings() {
	// horisontal ones
	let index = 0;
	let point1 = 0;
	let point2 = 1;
	for(let i = 0 ; i < heightOfCloth; ++i) {
		for(let j = 0; j < widthOfCloth - 1; ++j, ++index) {
			allSprings[index] = new Spring();
			point1 = i * widthOfCloth + j;
			point2 = i * widthOfCloth + j + 1;
			allSprings[index].setPoints(point1, point2);
			allPoints[point1].addConnected(point2);
			allPoints[point2].addConnected(point1);
		}
	}

	// vertical ones
	for(let i = 0 ; i < heightOfCloth -1 ; ++i) {
		for(let j = 0; j < widthOfCloth; ++j, ++index) {
			allSprings[index] = new Spring();			
			point1 = i * widthOfCloth + j;
			point2 = (i+1) * widthOfCloth + j;
			allSprings[index].setPoints(point1, point2)
			allPoints[point1].addConnected(point2);
			allPoints[point2].addConnected(point1);			
		}
	}
	//diagonal ones
	for(let i = 0 ; i < heightOfCloth -1 ; ++i) {
		for(let j = 0; j < widthOfCloth -1; ++j, ++index) {
			allSprings[index] = new Spring();			
			point1 = i * widthOfCloth + j;
			point2 = (i+1) * widthOfCloth + j + 1;
			allSprings[index].setPoints(point1, point2)
			allPoints[point1].addConnected(point2);
			allPoints[point2].addConnected(point1);			
		}
	}
}

var trianglesAmount = (widthOfCloth -1)* (heightOfCloth -1) * 2;
var frontTriangles = new Array(trianglesAmount);

function initTrinagles()  {
	var index = 0;
	for(let i = 0 ; i < heightOfCloth - 1; ++i) {
		for(let j = 0; j < widthOfCloth -1; ++j) {
			let current = i * widthOfCloth + j;
			let nextrow = (i + 1) * widthOfCloth + j;
			// front: 1-2-3
			// back: 1-3-2
			frontTriangles[index] = new Triangle(current, current + 1, nextrow +1);
			++index;
			//front: 1-2-3
			// back: 1-3-2
			frontTriangles[index] = new Triangle(current, nextrow +1, nextrow);
			++index;
		}
	}
}


var pointsCoordinatesSprings = new Array(springsLength * 3 * 2).fill(0.0);
var pointsColorsSprings = new Array(springsLength * 3 *2 ).fill(0.0);
var red = new Vector3d(1.0,0.0,0,0);

/*
объяснение: в нормальной ситуации были бы треугольники
сейчас надо менять цвет линии в зависимости от 
параметров пружины
решение: рисовать по линиям
минусы решения: будут видны невидимые грани
*/
function makeCoordsFromStrings () {
	let index = 0;
	for(spring of allSprings) {
		let firstLocation = allPoints[spring.getFirstPointIndex()].getCurrentLocation();		
		let secondLocation = allPoints[spring.getSecondPointIndex()].getCurrentLocation();		
		allPoints[spring.getFirstPointIndex()].clearForceMoved();
		allPoints[spring.getSecondPointIndex()].clearForceMoved();
		var color = new Vector3d(0.0,0,0);
		color.add(red);
		color.multiply(spring.getLengthDiffCoef());
		pointsCoordinatesSprings[index] = firstLocation.getX();
		pointsColorsSprings[index] = color.getX();
		++index;
		pointsCoordinatesSprings[index] = firstLocation.getY();
		pointsColorsSprings[index] = color.getY();
		++index;
		pointsCoordinatesSprings[index] = firstLocation.getZ();
		pointsColorsSprings[index] = color.getZ();
		++index;

		pointsCoordinatesSprings[index] = secondLocation.getX();
		pointsColorsSprings[index] = color.getX();
		++index;
		pointsCoordinatesSprings[index] = secondLocation.getY();
		pointsColorsSprings[index] = color.getY();
		++index;
		pointsCoordinatesSprings[index] = secondLocation.getZ();
		pointsColorsSprings[index] = color.getZ();
		++index;
	}
}


var pointsFrontTriangles = new Array(trianglesAmount * 3);
var normalsFrontTriangles = new Array(trianglesAmount * 3);

function insertPointFront(index, loc, normal) {
		pointsFrontTriangles[index] = loc.getX();
		normalsFrontTriangles[index] = normal.getX();
		++index;
		pointsFrontTriangles[index] = loc.getY();
		normalsFrontTriangles[index] = normal.getY();
		++index;
		pointsFrontTriangles[index] = loc.getZ();
		normalsFrontTriangles[index] = normal.getZ();
		++index;
}

function makeNormals() {
	for(point of allPoints) {
		point.clearNormal();
	}
	for(triangle of frontTriangles) {
		triangle.calculateNormal();
	}
	for(point of allPoints) {
		point.normal.normalize();
	}
}
function makeCoordsTriangles() {
	var index = 0;
	for(triangle of frontTriangles) {
		var firstLoc = allPoints[triangle.getFirst()].getCurrentLocation();
		var firstNormal = allPoints[triangle.getFirst()].getNormal();
		var secondLoc = allPoints[triangle.getSecond()].getCurrentLocation(); 
		var secondNormal = allPoints[triangle.getSecond()].getNormal();
		var thirdLoc = allPoints[triangle.getThird()].getCurrentLocation();
		var thirdNormal = allPoints[triangle.getThird()].getNormal();
		
		// front: 1-2-3, back: 1-3-2
		// normal: front +1, back -1;
		insertPointFront(index, firstLoc, firstNormal);
		index +=3;
		insertPointFront(index, secondLoc, secondNormal);
		index +=3;
		insertPointFront(index, thirdLoc, thirdNormal);
		index +=3;
	}
	index = 0;	
}
let allMovementTime = 0;
function moveMiddlePoint(time) {
	let movement= new Vector3d(0,-0.5,0);
	time = time * allMovementTime * 20;
	allMovementTime+=1;
	movement.multiply(Math.sin(time * Math.PI / 180));
	movement.add(allPoints[middleIndex].getBaseLocation());
	allPoints[middleIndex].setLocation(movement);
}


function collidePoints() {
	var index,index1;
	for(index = 0; index < pointsCount;++index) {
		for(index1 = index +1; index1 < pointsCount; ++index1) {
			allPoints[index].collide(index1,allPoints[index1]);
		}
	}
}
function proceedPoints(time) {
	for(spring of allSprings) {
		spring.proceed(time);
	}
	for(point of allPoints) {
		point.proceed(time);
	}
	collidePoints();	
	for(point of allPoints) {
		point.clearForceMoved();
	}	
	for(spring of allSprings) {
		spring.stretch();
	}
	moveMiddlePoint(time);
	makeNormals();
	makeCoordsFromStrings();	
	makeCoordsTriangles();
}

function prepareCloth() {
	initPoints();
	initSprings();
	initTrinagles();
	makeNormals();
	makeCoordsFromStrings();
	makeCoordsTriangles();
}


// скопировано с сайта-туториала
function createShader(gl, type, source) {
	var shader = gl.createShader(type);
	gl.shaderSource(shader, source);
	gl.compileShader(shader);
	var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
	if (success) {
		return shader;
	}
	console.log(gl.getShaderInfoLog(shader));
	gl.deleteShader(shader);
}


// скопировано с сайта-туториала
function createProgram(gl, vertexShader, fragmentShader) {
	var program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);
	var success = gl.getProgramParameter(program, gl.LINK_STATUS);
	if (success) {
		return program;
	}
	console.log(gl.getProgramInfoLog(program));
	gl.deleteProgram(program);
}

var isFillActive = true;

class Cloth {
	constructor() {
		this.gl = canvas.getContext("webgl");
		var gl = this.gl;
		if (gl === null) {
			alert("Unable to initialize WebGL. Your browser or machine may not support it.");
			return;
		}
		this.locations = {};
		this.locationsTriangle = {};
		this.transforms = {};
		this.shaders = {};
		this.camera = {};
		prepareCloth();
		var vertexShaderSource = document.querySelector("#vertex-shader").text;
		this.vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
		
		var vertexShaderTrinagleSource = document.querySelector("#vertex-shader-with-light").text;
		this.vertexShaderTriangle = createShader(gl, gl.VERTEX_SHADER, vertexShaderTrinagleSource);
		
		
		var fragmentShaderSource = document.querySelector("#fragment-shader").text;
		this.fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
		
		var fragmentShaderSourceTriangle = document.querySelector("#fragment-shader-with-light").text;
		this.fragmentShaderTriangle = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSourceTriangle);

		this.program = createProgram(gl, this.vertexShader, this.fragmentShader);		
		this.programTriangle = createProgram(gl,this.vertexShaderTriangle,this.fragmentShaderTriangle);
		this.buffers = {};
			
		this.transforms.model = m4.identify();
		this.camera.position = new Vector3d(0,5,4);
		this.camera.direction = new Vector3d(0,0,0);
		this.camera.horizontal = Math.PI / 6;
		this.camera.vertical = Math.PI / 3 ;
		this.transforms.proj = m4.perspective(Math.PI /4, canvas.width / canvas.height, 1, 100);
		this.computeView();		
	}
	initGl() {
		var gl = this.gl;
		this.locations.position = gl.getAttribLocation(this.program, "a_position");
		this.locations.color  = gl.getAttribLocation(this.program, "a_color");		
		this.locations.mvp = gl.getUniformLocation(this.program, "mvp_matrix");
		this.buffers.position = gl.createBuffer();
		this.buffers.color = gl.createBuffer();	
		//this.buffers.normals = gl.createBuffer();
		
		this.locationsTriangle.position = gl.getAttribLocation(this.programTriangle, "a_position");
		this.locationsTriangle.normal = gl.getAttribLocation(this.programTriangle, "a_normal");
		this.locationsTriangle.mvp = gl.getUniformLocation(this.programTriangle, "mvp_matrix");
		this.locationsTriangle.u_normalMatrix = gl.getUniformLocation(this.programTriangle, "u_normalMatrix");
		this.locationsTriangle.u_color = gl.getUniformLocation(this.programTriangle, "u_color");
		this.locationsTriangle.u_direction = gl.getUniformLocation(this.programTriangle, "u_direction");
		this.locationsTriangle.u_cameraPosition = gl.getUniformLocation(this.programTriangle, "u_cameraPosition");


	}
	setUniforms() {
		var gl = this.gl;
		gl.useProgram(this.program);
		gl.uniformMatrix4fv(this.locations.mvp, false, this.transforms.mvp);
		
		gl.useProgram(this.programTriangle);
		gl.uniformMatrix4fv(this.locationsTriangle.mvp, false, this.transforms.mvp);
		gl.uniformMatrix4fv(this.locationsTriangle.u_normalMatrix, false, this.transforms.model);
		gl.uniform3fv(this.locationsTriangle.u_color, [0.0,1.0,0.0]);
		gl.uniform3fv(this.locationsTriangle.u_direction,  this.camera.direction.data);
		gl.uniform3fv(this.locationsTriangle.u_cameraPosition, this.camera.position.data);
		

	}
	setAttributes() {
		var gl = this.gl;
		gl.useProgram(this.program);
		gl.enableVertexAttribArray(this.locations.position);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.position);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(pointsCoordinatesSprings), gl.DYNAMIC_DRAW);
		
		// Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
		var size = 3;					
		var type = gl.FLOAT;	 // the data is 32bit floats
		var normalize = false; // don't normalize the data
		var stride = 0;				// 0 = move forward size * sizeof(type) each iteration to get the next position
		var offset = 0;				// start at the beginning of the buffer
		gl.vertexAttribPointer(this.locations.position, size, type, normalize, stride, offset);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.color);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(pointsColorsSprings), gl.DYNAMIC_DRAW);
		gl.enableVertexAttribArray(this.locations.color);	
		gl.vertexAttribPointer(this.locations.color, size, type, normalize, stride, offset);
	}
	setAttributesTriangles() {
		var gl = this.gl;
		gl.useProgram(this.programTriangle);
		gl.enableVertexAttribArray(this.locationsTriangle.position);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.position);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(pointsFrontTriangles), gl.DYNAMIC_DRAW);
		
		// Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
		var size = 3;					
		var type = gl.FLOAT;	 // the data is 32bit floats
		var normalize = false; // don't normalize the data
		var stride = 0;				// 0 = move forward size * sizeof(type) each iteration to get the next position
		var offset = 0;				// start at the beginning of the buffer	
		gl.vertexAttribPointer(this.locationsTriangle.position, size, type, normalize, stride, offset);
		
		gl.enableVertexAttribArray(this.locationsTriangle.normal);
		gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.color);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normalsFrontTriangles), gl.DYNAMIC_DRAW);		
		gl.vertexAttribPointer(this.locationsTriangle.normal, size, type, normalize, stride, offset);

	}
	prepare() {
		prepareCloth();
		this.initGl();
		this.setUniforms();
	}
	drawLines() {
		makeCoordsFromStrings();
		this.setAttributes();
		this.setUniforms();
		var gl = this.gl;		
		gl.useProgram(this.program);
		var primitiveType = gl.LINES;
		var count = springsLength *2;
		gl.drawArrays(primitiveType, 0, count);
		
	}
	drawTriangles() {
		makeCoordsTriangles();
		this.setAttributesTriangles();
		var gl = this.gl;		
		gl.useProgram(this.programTriangle);
		var primitiveType = gl.TRIANGLES;
		var count = trianglesAmount *3;
		gl.drawArrays(primitiveType, 0, count);
	}
	draw() {
		this.setAttributes();
		this.setUniforms();
		var gl = this.gl;
		gl.enable(gl.DEPTH_TEST);
		gl.clearColor(0.8, 0.8, 0.8, 1.0);
		gl.clear(gl.COLOR_BUFFER_BIT);
		if(isFillActive) {
			this.drawTriangles();
		}
		this.drawLines();		
		proceedPoints(0.025);
		requestAnimationFrame(this.draw.bind(this))
	}
	computeView() {
		this.camera.direction = new Vector3d(Math.sin(this.camera.horizontal) * Math.cos(this.camera.vertical),
								Math.sin(this.camera.vertical),
								Math.cos(this.camera.horizontal) * Math.cos(this.camera.vertical));
		var right = new Vector3d(Math.sin(this.camera.horizontal - Math.PI /2 ),
					0,
					Math.cos(this.camera.horizontal  - Math.PI /2 ));
		this.camera.up = this.camera.direction.cross(right);
		this.camera.direction.add(this.camera.position);
		this.transforms.view = m4.lookAt(this.camera.position,this.camera.direction,this.camera.up);
		this.transforms.mvp = m4.multiply(this.transforms.proj,this.transforms.view);
		this.transforms.mvp = m4.multiply(this.transforms.mvp, this.transforms.model);
		this.camera.direction.sub(this.camera.position);
	}
	updateView() {
		this.computeView();
		this.setUniforms();
	}
}
// сделано по сайту-туториалу.
var canvas = document.querySelector("#glCanvas");
var cloth;
function main() {
	
	canvas = document.querySelector("#glCanvas");
	cloth = new Cloth;
	cloth.prepare();
	cloth.draw();
}

function toggleGravity() {
	var checkBox = document.getElementById("gravityToggler");
	isGravityActive = checkBox.checked;
}

function toggleFill() {
	var checkBox = document.getElementById("trianglesToggle");
	isFillActive = checkBox.checked;
}

function  togglePositionX() {
	var source = document.getElementById("positionToggleX");
	var value = parseInt(source.value)
	if(!isNaN(value)) {
		cloth.camera.position.setX(value);
		cloth.computeView();
	}
}

function  togglePositionY() {
	var source = document.getElementById("positionToggleY");
	var value = parseInt(source.value)
	if(!isNaN(value)) {
		cloth.camera.position.setY(value);
		cloth.computeView();
	}
}
function  togglePositionZ() {
	var source = document.getElementById("positionToggleZ");
	var value = parseInt(source.value)
	if(!isNaN(value)) {
		cloth.camera.position.setZ(value);
		cloth.computeView();
	}
}

function togglePitch() {
	var source = document.getElementById("rotationPitch");
	var value = parseFloat(source.value)
	if(!isNaN(value)) {
		value = value * Math.PI / 180;
		cloth.camera.horizontal = value;
		cloth.computeView();
	}
}
function toggleYaw() {
	var source = document.getElementById("rotationYaw");
	var value = parseFloat(source.value)
	if(!isNaN(value)) {
		value = value * Math.PI / 180;
		cloth.camera.vertical = value;
		cloth.computeView();
	}
}
window.onload = main;
